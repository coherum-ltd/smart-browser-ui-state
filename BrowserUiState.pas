{
  BrowserUiState - SmartPascal bindings
  Copyright (C) 2021  Nikola Dimitrov
    
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software Foundation,
  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA

  This software uses the library
  [browser-ui-state](https://www.npmjs.com/package/browser-ui-state).
  Refer to [LICENSE](https://github.com/device-hackers/browser-ui-state/blob/master/LICENSE)
}

unit BrowserUiState;

interface

uses
  W3C.HTML5;

{$R 'browser-ui-state.js'}

type
  TOrientation = String;
  TOrientationHelper = strict helper for TOrientation
    const LANDSCAPE = 'LANDSCAPE';
    const PORTRAIT = 'PORTRAIT';
  end;

  TState = String;
  TStateHelper = strict helper for TState
    const COLLAPSED = 'COLLAPSED';
    const EXPANDED = 'EXPANDED';
    const KEYBOARD = 'KEYBOARD';
    const KEYBOARD_NO_RESIZ = 'KEYBOARD_NO_RESIZE';
    const HTML5_FULLSCREEN = 'HTML5_FULLSCREEN';
    const SPLIT_MODE = 'SPLIT_MODE';
    const HTML5_FULLSCREEN_IN_SPLIT_MODE = 'HTML5_FULLSCREEN_IN_SPLIT_MODE';
    const DESKTOP = 'DESKTOP';
    const DESKTOP_HTML5_FULLSCREEN = 'DESKTOP_HTML5_FULLSCREEN';
    const STATIC = 'STATIC';
    const SAFARI_HOMESCREEN = 'SAFARI_HOMESCREEN';
    const UNKNOWN = 'UNKNOWN';
  end;

  JBrowserUiState = class external 'BrowserUiState'
  public
    FOrientation: TOrientation; external 'orientation';
    FScreenAspectRatio: Float; external 'screenAspectRatio';
    FViewportAspectRatio: Float; external 'viewportAspectRatio';
    FDelta: Integer; external 'delta';
    FDeviation: Float; external 'deviation';
    FCollapsedThreshold: Float; external 'collapsedThreshold';
    FKeyboardThreshold: Float; external 'keyboardThreshold';
    FState: TState; external 'state';
    FFScreen: Variant; external 'fscreen';

    constructor Create; overload;
    constructor Create(initialOrientation: TOrientation; win: JWindow); overload;

    property Orientation: TOrientation read FOrientation write FOrientation;
    property ScreenAspectRatio: Float read FScreenAspectRatio write FScreenAspectRatio;
    property ViewportAspectRatio: Float read FViewportAspectRatio write FViewportAspectRatio;
    property Delta: Integer read FDelta write FDelta;
    property Deviation: Float read FDeviation write FDeviation;
    property CollapsedThreshold: Float read FCollapsedThreshold write FCollapsedThreshold;
    property KeyboardThreshold: Float read FKeyboardThreshold write FKeyboardThreshold;
    property State: TState read FState write FState;
    property FScreen: Variant read FFScreen write FFScreen;

  protected

  private

  end;

implementation

initialization

finalization

end.
